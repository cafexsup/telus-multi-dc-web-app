<?php

header('Access-Control-Allow-Origin: *');

function generateRandomString($length = 16) {
    return substr(str_shuffle(str_repeat($x='0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ', ceil($length/strlen($x)) )),1,$length);
}

$cid = generateRandomString(); 

$url = "https://172.31.250.62:8443";

//CONSUMER

    $consumer = '
        {
            "webAppId": "webapp-id-example",
            "allowedOrigins": ["*"],
            "urlSchemeDetails": {
                "host": "172.31.250.62",
                "port": "8443",
                "secure": true
            },
            "voice":
            {
                "username": "%s",
                "displayName": "consumer",
                "domain": "172.31.250.62",
                "inboundCallingEnabled": true
            },

            "aed": {
                "accessibleSessionIdRegex": ".*",
                "maxMessageAndUploadSize": "5000",                                                                                       "dataAllowance": "5000"                                                                   },
                "additionalAttributes":{
                "AED2.metadata":{
                    "role":"consumer"
                },
            "AED2.allowedTopic":"%s"
            }
        }
    ';

        // if supplied, use the account details provided
    $username = (empty($_GET['username'])) ? 'username'   : $_GET['username'];
    // $cid = (empty($_GET['cid'])) ? '1234567890'   : $_GET['cid'];
    $consumer = sprintf($consumer, $username, $cid);


    // configure the curl options
    $consumer_ch = curl_init("http://172.31.250.62:8080/gateway/sessions/session");
    curl_setopt($consumer_ch,CURLOPT_POST, true);
    curl_setopt($consumer_ch,CURLOPT_POSTFIELDS, $consumer);
    curl_setopt($consumer_ch, CURLOPT_RETURNTRANSFER, true);
    curl_setopt($consumer_ch, CURLOPT_SSL_VERIFYPEER, false);    
    curl_setopt($consumer_ch, CURLOPT_HTTPHEADER, [         
        'Content-Type: application/json',
        'Content-Length: ' . strlen($consumer)
    ]);

    // execute HTTP POST & close the connection
    $consumer_response = curl_exec($consumer_ch);
    curl_close($consumer_ch);

    // decode the JSON and pick out the session token
    $consumerDecodedJson = json_decode($consumer_response);
    $consumerId = $consumerDecodedJson->{'sessionid'};

    //Agent

     // configure the JSON to use in the session.
    $agent = '
        {
            "webAppId": "webapp-id-example",
            "allowedOrigins": ["*"],
            "urlSchemeDetails": {
                "host": "172.31.250.62",
                "port": "8443",
                "secure": true
            },
            "voice":
            {
                "username": "%s",
                "displayName": "Agent",
                "domain": "172.31.250.62",
                "inboundCallingEnabled": true
            },

            "aed": {
                "accessibleSessionIdRegex": ".*",
                "maxMessageAndUploadSize": "5000",                                                                                       "dataAllowance": "5000"                                                                   },
                "additionalAttributes":{
                "AED2.metadata":{
                    "role":"agent"
                },
            "AED2.allowedTopic":"%s"
            }
        }
    ';

    // if supplied, use the account details provided
    // $cid = (empty($_GET['cid'])) ? '1234567890'   : $_GET['cid'];
    $agent = sprintf($agent, $cid, $cid);

    // configure the curl options
    $ch = curl_init("http://172.31.250.62:8080/gateway/sessions/session");
    curl_setopt($ch,CURLOPT_POST, true);
    curl_setopt($ch,CURLOPT_POSTFIELDS, $agent);
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
    curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);    
    curl_setopt($ch, CURLOPT_HTTPHEADER, [         
        'Content-Type: application/json',
        'Content-Length: ' . strlen($agent)
    ]);

    // execute HTTP POST & close the connection
    $response = curl_exec($ch);
    curl_close($ch);

    // decode the JSON and pick out the session token
    $decodedJson = json_decode($response);
    $agentId = $decodedJson->{'sessionid'};

    $json = '{"consumerToken":"%s","agentToken":"%s","cid":"%s","url":"%s"}';


    $json = sprintf($json, $consumerId, $agentId, $cid, $url);

    // echo the ID we've retrieved
    echo $json;


?>
